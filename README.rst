OpenStreetMap survey round trip
===============================

Tool to plan survey trip from control target points (`notes`_, `quality assurance warnings`_)

.. _notes: https://wiki.openstreetmap.org/wiki/Notes
.. _quality assurance warnings: https://wiki.openstreetmap.org/wiki/Osmose


Dependencies
------------

* libgdal-dev
* Python >= 3.6
* python-osrm


Usage
-----

* Input:
  * AoI (ex.: r1663798)
  * start (ex.: `-52.3311,4.8503 <geo:4.8503,-52.3311>`_)
* Output: GPX route
* Example:

    survey r1663798 -52.3311,4.8503 notes,err_1,err_2,note_tag,fixme,fr_voie_adr,fr_voie,fr_loc
    
    python __main__.py -- r1663798 -52.3311,4.8503 notes

.. _france_guyane: http://download.geofabrik.de/europe/france/guyane.html


Notes
-----

* OSRM
  * trip, roundtrip, source=first
  * output: geometries, overviews?
  * roundtrip=true, source=first, destination=any
  
  
TODO
----

* Get points
  1. Cadastre way addr
  2. Cadastre locality
  3. Cadastre way
  4. fixme=
  5. note=
  6. Notes
  7. Osmose level 1
  8. Osmose level 2
* Get AoI from start position
* Docker set database (see Osmose Dockerfile and docker-compose.yml)
* Pre-filter points with OSM ways 1km buffer
