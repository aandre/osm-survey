#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 19:17:06 2019

@author: aandre
"""
import os
import logging

import json
import requests

import csv

import argparse

import attr

import re

import configparser


logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))

HEADERS = {"User-Agent": "OSMQA Survey/0.1"}

TOLERANCE = 0.0001

PATTERN_LOT = "^LOT\s"
PATTERN_RES = "^RES\s"

PATTERN_CARD = ".+\s(NORD|SUD|OUEST|EST)$"


@attr.s(auto_attribs=True)
class Control(object):
    lon: float
    lat: float

    name: str

    ctrl_type: str

    identifier: str

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "name": self.name,
            "type": self.ctrl_type,
            "identifier": self.identifier
          }
        }


def controls_to_geojson(controls):
    return {
        "type": "FeatureCollection",
        "features": [control.to_geojson() for control in controls]
        }


def is_geo(o):
    return abs(o.lon) > TOLERANCE and abs(o.lat) > TOLERANCE


@attr.s(auto_attribs=True, frozen=True)
class Way(object):
    code: str
    nom: str = None
    nom_osm: str = None

    lon: float = 0.0
    lat: float = 0.0

    statut: int = 0

    housenumbers: int = 0

    def is_doublon(self):
        return re.match(PATTERN_CARD, self.nom)

    def __lt__(self, other):

        if self.housenumbers == other.housenumbers:
            lt = self.nom.__lt__(other.nom)
        else:
            lt = self.housenumbers < other.housenumbers

        return lt

    def is_lotissement(self):
        return re.match(PATTERN_LOT, self.nom)

    def is_residence(self):
        return re.match(PATTERN_RES, self.nom)

    def to_control(self, category="way"):
        return Control(self.lon, self.lat, self.nom, category, self.code)

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "code": self.code,
            "nom": self.nom,
            "nom_osm": self.nom_osm,
            "statut": self.statut,
            "housenumbers": self.housenumbers
          }
        }

    @staticmethod
    def from_list(js_list):
        code, date_str, nom_raw, nom_osm_raw, lon_str, lat_str, statut_str = js_list[:7]

        nom = nom_raw.strip()
        nom_osm = nom_osm_raw.strip() if nom_osm_raw != "--" else ""

        lon = float(lon_str if lon_str else 0)
        lat = float(lat_str if lat_str else 0)

        statut = int(statut_str)

        housenumbers = 0

        if len(js_list) == 8:
            nums_str = js_list[7]
            housenumbers = int(nums_str)

        return Way(code, nom, nom_osm, lon, lat, statut, housenumbers)  # TODO: Add date


@attr.s(auto_attribs=True)
class Locality(object):
    code: str
    nom: str = None
    nom_osm: str = None

    lon: float = 0.0
    lat: float = 0.0

    statut: int = 0

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "code": self.code,
            "nom": self.nom,
            "nom_osm": self.nom_osm
          }
        }

    def is_doublon(self):
        return re.match(PATTERN_CARD, self.nom)

    def to_control(self):
        return Control(self.lon, self.lat, self.nom, "locality", self.code)

    @staticmethod
    def from_list(js_list):
        code, date_str, nom_raw, nom_osm_raw, lon_str, lat_str, statut_str = js_list[:7]

        nom = nom_raw.strip()
        nom_osm = nom_osm_raw.strip() if nom_osm_raw != "--" else ""

        lon = float(lon_str if lon_str else 0)
        lat = float(lat_str if lat_str else 0)

        statut = int(statut_str)

        return Locality(code, nom, nom_osm, lon, lat, statut)  # TODO: Add date


def locality_to_geojson(localities):
    return {
        "type": "FeatureCollection",
        "features": [locality.to_geojson() for locality in localities]
        }


@attr.s(auto_attribs=True)
class Tag(object):
    element_type: str
    identifier: int

    lon: float = 0.0
    lat: float = 0.0

    text: str = None

    def code(self):
        return "%s%s" % (self.element_type[0], self.identifier)

    def __eq__(self, other):
        return self.element_type == other.element_type and self.identifier == other.identifier

    def to_control(self, category="tag"):
        return Control(self.lon, self.lat, self.text, category, self.code())

    @staticmethod
    def from_list(cells):
        lon_str, lat_str, element_type, identifier_str, text = cells

        identifier = int(identifier_str)

        lon = float(lon_str if lon_str else 0)
        lat = float(lat_str if lat_str else 0)

        return Tag(element_type, identifier, lon, lat, text)

    @staticmethod
    def from_exclusion(text):
        tag = Tag()

        return tag


@attr.s(auto_attribs=True)
class Note(object):
    identifier: int

    lon: float = 0.0
    lat: float = 0.0

    text: str = None

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "id": self.identifier,
            "text": self.text
          }
        }

    def url(self):
        return "https://osm.org/note/%s" % self.identifier

    @staticmethod
    def from_json(json):
        lon, lat = json["geometry"]["coordinates"]

        return Note(json["properties"]["id"], lon, lat, json["properties"]["comments"][0]["text"])

    def to_control(self):
        return Control(self.lon, self.lat, self.text, "note", self.identifier)


@attr.s(auto_attribs=True)
class Error(object):
    identifier: int

    err_class: int = 0
    item: str = 0

    lon: float = 0.0
    lat: float = 0.0

    title: str = None
    subtitle: str = None

    level: int = 0

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "identifier": self.identifier,
            "err_class": self.err_class,
            "item": self.item,
            "title": self.title,
            "subtitle": self.subtitle
          }
        }

    def url(self):
        return "http://osmose.openstreetmap.fr/en/error/%s" % self.identifier

    @staticmethod
    def from_list(js_list):
        lat_str, lon_str, id_str, item_str, source_str, class_str, __, __, subtitle, title, level_str = js_list[:11]

        identifier = int(id_str)
        err_class = int(class_str)
        item = int(item_str)

        level = int(level_str)

        lon = float(lon_str if lon_str else 0)
        lat = float(lat_str if lat_str else 0)

        return Error(identifier, err_class, item, lon, lat, title, subtitle, level)

    def to_control(self):
        return Control(self.lon, self.lat, self.title, "error%s" % self.level, self.identifier)


def errors_to_geojson(errors):
    return {
        "type": "FeatureCollection",
        "features": [error.to_geojson() for error in errors]
        }


@attr.s(auto_attribs=True)
class Waypoint(object):
    lon: float
    lat: float

    index: int

    name: str

    def to_geojson(self):
        return {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [self.lon, self.lat]
          },
          "properties": {
            "index": self.index,
            "name": self.name
          }
        }

    @staticmethod
    def from_json(jso):
        lon, lat = jso["location"]

        return Waypoint(lon, lat, jso["waypoint_index"], jso["name"])


def waypoints_to_geojson(waypoints):
    return {
        "type": "FeatureCollection",
        "features": [waypoint.to_geojson() for waypoint in waypoints]
        }


def get_bbox(insee):
    url = "http://overpass-api.de/api/interpreter?data=[out:json]; relation[\"ref:INSEE\"=\"%s\"]; out bb;" % insee

    response = requests.get(url)
    data = json.loads(response.text)

    box = data["elements"][0]["bounds"]

    # left, bottom, right, top = box
    return box["minlon"], box["minlat"], box["maxlon"], box["maxlat"]


def aoi_valid(aoi):

    return re.match(aoi, r"^[wr][0-9]+$")


def get_fantoir(insee):
    url = "https://bano.openstreetmap.fr/fantoir/requete_fantoir.py?insee=%s" % insee

    response = requests.get(url)

    return json.loads(response.text)


def get_ways_addr(fantoir_data):
    ways_js = fantoir_data[1]

    # Voies à adresses non rapprochées
    ways = [Way.from_list(way_js) for way_js in ways_js]

    ways = [way for way in ways if is_geo(way)]
    ways = [way for way in ways if not way.is_residence()]
    ways = [way for way in ways if not way.is_lotissement()]

    # Statut
    ways = [way for way in ways if way.statut == 0]

    return ways


def get_ways(fantoir_data):
    ways_js = fantoir_data[3]

    # Voies à adresses non rapprochées
    ways = [Way.from_list(way_js) for way_js in ways_js]

    ways = [way for way in ways if is_geo(way)]
    ways = [way for way in ways if not way.is_residence()]
    ways = [way for way in ways if not way.is_lotissement()]

    # Statut
    ways = [way for way in ways if way.statut == 0]

    return ways


def get_localities(fantoir_data):
    localities_js = fantoir_data[5]

    # Voies à adresses non rapprochées
    localities = [Locality.from_list(locality_js) for locality_js in localities_js]

    localities = [locality for locality in localities if is_geo(locality)]
    localities = [locality for locality in localities if not locality.is_doublon()]

    # Statut
    localities = [locality for locality in localities if locality.statut == 0]

    return localities


def get_tags(name, bbox):
    left, bottom, right, top = bbox
    url = "http://overpass-api.de/api/interpreter?data=[out:csv(::lon, ::lat, ::type, ::id, \"%s\"; true; \";\")][bbox:%s,%s,%s,%s]; nwr[%s]; out center;" % (name, bottom, left, top, right, name)
    logging.debug("Retrieving tags %s", url)

    response = requests.get(url, headers=HEADERS)
    text_data = response.text.splitlines()
    # logging.debug("Overpass data %s", text_data)

    reader = csv.reader(text_data, delimiter=";")

    return [Tag.from_list(line) for line in list(reader)[1:]]


def get_notes(bbox):
    left, bottom, right, top = bbox
    url = "https://api.openstreetmap.org/api/0.6/notes.json?closed=0&bbox=%s,%s,%s,%s" % (left, bottom, right, top)
    logging.debug("Retrieving notes %s", url)

    response = requests.get(url, headers=HEADERS)
    data = json.loads(response.text)

#    with open("notes.json", "r") as read_file:
#        data = json.load(read_file)

    return [Note.from_json(jso) for jso in data["features"]]


def get_errors(bbox, level, limit=25):
    left, bottom, right, top = bbox
    url = "http://osmose.openstreetmap.fr/fr/api/0.2/errors?full=true&level=%s&bbox=%s,%s,%s,%s&limit=%s" % (level, left, bottom, right, top, limit)
    logging.debug("Retrieving errors %s", url)

    response = requests.get(url, headers=HEADERS)
    data = json.loads(response.text)

#    with open("errors.json", "r") as read_file:
#        data = json.load(read_file)

    return [Error.from_list(jso) for jso in data["errors"]]


def get_excluded(configfile):
    excluded = {}

    EXCLUDES = configparser.ConfigParser()
    EXCLUDES.read(configfile)

    if "way_addr" in EXCLUDES:
        way_addr = EXCLUDES["way_addr"]
        if "excluded" in way_addr:
            exclusions = way_addr["excluded"]
            excluded["way_addr"] = set(exclusions.split('\n'))

    if "locality" in EXCLUDES:
        locality = EXCLUDES["locality"]
        if "excluded" in locality:
            exclusions = locality["excluded"]
            excluded["locality"] = set(exclusions.split('\n'))

    if "way" in EXCLUDES:
        way = EXCLUDES["way"]
        if "excluded" in way:
            exclusions = way["excluded"]
            excluded["way"] = set(exclusions.split('\n'))

    if "tag_fixme" in EXCLUDES:
        tag_fixme = EXCLUDES["tag_fixme"]
        if "excluded" in tag_fixme:
            exclusions = tag_fixme["excluded"]
            excluded["tag_fixme"] = set(exclusions.split('\n'))

    if "tag_note" in EXCLUDES:
        tag_note = EXCLUDES["tag_note"]
        if "excluded" in tag_note:
            exclusions = tag_note["excluded"]
            excluded["tag_note"] = set(exclusions.split('\n'))

    if "note" in EXCLUDES:
        note = EXCLUDES["note"]
        if "excluded" in note:
            exclusions = note["excluded"]
            excluded["note"] = set([int(nid) for nid in exclusions.split('\n')])

    if "error_l1" in EXCLUDES:
        error_l1 = EXCLUDES["error_l1"]
        if "excluded" in error_l1:
            exclusions = error_l1["excluded"]
            excluded["error_l1"] = set([int(eid) for eid in exclusions.split('\n')])

    if "error_l2" in EXCLUDES:
        error_l2 = EXCLUDES["error_l2"]
        if "excluded" in error_l2:
            exclusions = error_l2["excluded"]
            excluded["error_l2"] = set([int(eid) for eid in exclusions.split('\n')])

    return excluded


def osrm_url(start, controls):
    base = "http://router.project-osrm.org"
    service, version, profile = "trip", "v1", "driving"

    start_coords = "%s,%s" % start
    coordinates = start_coords + ";" + ";".join(["%s,%s" % (point.lon, point.lat) for point in controls])
    params = "geometries=geojson&overview=simplified"

    return "%s/%s/%s/%s/%s?%s" % (base, service, version, profile, coordinates, params)


def get_trip(start, controls):
    url = osrm_url(start, controls)
    logging.debug("URL: %s" % url)

    response = requests.get(url, headers=HEADERS)
    data = json.loads(response.text)

#    with open("trip.json", "r") as read_file:
#        data = json.load(read_file)

    overview = data["trips"][0]["geometry"]
    waypoints = [Waypoint.from_json(jso) for jso in data["waypoints"]]

    return overview, waypoints


def save_point(point, filename):

    jso = {
  "type": "FeatureCollection",
  "name": "point",
  "crs": {
    "type": "name",
    "properties": {
      "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
    }
  },
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Point",
        "coordinates": list(point)
      }
    }
  ]
}

    with open(filename, "w") as write_file:
        json.dump(jso, write_file)


def save_overview(overview, filename):

    jso = {
  "type": "FeatureCollection",
  "name": "line",
  "crs": {
    "type": "name",
    "properties": {
      "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
    }
  },
  "features": [
    {
      "type": "Feature",
      "properties": {
        "id": 0
      },
      "geometry": overview
    }
  ]
}

    with open(filename, "w") as write_file:
        json.dump(jso, write_file)


if __name__ == "__main__":
    # execute only if run as a script

    PARSER = argparse.ArgumentParser(
            description="Survey roundtrip.",
            prog="survey")
    PARSER.add_argument("AoI",
                        help="Area of Interest (admin zone). Ex.: r1663798")
    PARSER.add_argument("start",
                        help="Start location (lon, lat). Ex.: -52.3311,4.8503")
    PARSER.add_argument("layer",
                        help="Points to control. Ex.: notes", default="notes")
    ARGS = PARSER.parse_args()

    AOI = ARGS.AoI
    START = ARGS.start
    LAYER = ARGS.layer

    # polygon =

    # gadm36_GUF.gpkg|layername=gadm36_GUF_2
    # "HASC_2" in ('GF.CY.IR', 'GF.CY.KR', 'GF.CY.MC', 'GF.CY.MY', 'GF.CY.MT', 'GF.CY.OU', 'GF.CY.RK', 'GF.CY.RM', 'GF.CY.RO', 'GF.CY.SG', 'GF.CY.SI', 'GF.SL.AY', 'GF.SL.MN', 'GF.SL.SL')

    # AoI bbox
    # bbox = bbox(AOI)
    # bbox = (-54.61, 2.11, -51.63, 5.76)  # Guyane

    insee = 97307
    bbox = (-52.411, 4.738, -52.287, 4.916)  # get_bbox(insee)
    start = (-52.3631, 4.9013)

    # Get OSM-cadastre comparison
    fantoir_data = get_fantoir(insee)

    # Missing ways with addresses
    # ways_addr = []
    ways_addr = get_ways_addr(fantoir_data)
    ways_addr.sort(reverse=True)
    logging.debug("Ways addr: %s" % ways_addr[:1])

    # Missing localities
    # localities = []
    localities = get_localities(fantoir_data)
    logging.debug("Localities: %s" % localities[:1])

    # Missing ways
    # ways = []
    ways = get_ways(fantoir_data)
    logging.debug("Ways: %s" % ways[:1])

    # FIXMEs
    # tag_fixmes = []
    tag_fixmes = get_tags("fixme", bbox)
    logging.debug("FIXMEs: %s" % tag_fixmes[:1])

    # note tags
    # tag_notes = []
    tag_notes = get_tags("note", bbox)
    logging.debug("Tag notes: %s" % tag_notes[:1])

    # Notes
    # notes = []
    notes = get_notes(bbox)
    logging.debug("Notes: %s" % notes[:1])

    # Level 1 Osmose error
    # errors_l1 = []
    errors_l1 = get_errors(bbox, 1, 10)
    logging.debug("Errors L1: %s" % errors_l1[:1])

    # Level 1 Osmose error
    # errors_l2 = []
    errors_l2 = get_errors(bbox, 2, 10)
    logging.debug("Errors L2: %s" % errors_l2[:1])

    # TODO: Get building=*, name=* (overpass-turbo.eu/s/tuT)


    # TODO: Exlude errors (id, class-item)
    excluded = get_excluded("exclude.cfg")

    if "way_addr" in excluded:
        ways_addr = [way for way in ways_addr if not way.code in excluded["way_addr"]]
    if "locality" in excluded:
        localities = [locality for locality in localities if not locality.code in excluded["locality"]]
    if "way" in excluded:
        ways = [way for way in ways if not way.code in excluded["way"]]
    if "tag_fixme" in excluded:
        tag_fixmes = [fixme for fixme in tag_fixmes if not fixme.code() in excluded["tag_fixme"]]
    if "tag_note" in excluded:
        tag_notes = [note for note in tag_notes if not note.identifier in excluded["tag_note"]]
    if "note" in excluded:
        notes = [note for note in notes if not note.identifier in excluded["note"]]
    if "error_l1" in excluded:
        errors_l1 = [error for error in errors_l1 if not error.identifier in excluded["error_l1"]]
    if "error_l2" in excluded:
        errors_l2 = [error for error in errors_l2 if not error.identifier in excluded["error_l2"]]

    controls = []  # controls = get_controls()
    controls = controls + [way.to_control("way_addr") for way in ways_addr[:4]]
    controls = controls + [locality.to_control() for locality in localities[:4]]
    controls = controls + [way.to_control("way") for way in ways[:4]]
    controls = controls + [fixme.to_control("tag_fixme") for fixme in tag_fixmes[:4]]
    controls = controls + [note.to_control("tag_note") for note in tag_notes[:4]]
    controls = controls + [note.to_control() for note in notes[:4]]
    controls = controls + [error.to_control() for error in errors_l1[:4]]
    controls = controls + [error.to_control() for error in errors_l2[:4]]
    logging.debug("Controls: %s" % controls[:1])

    if controls:
        controls_js = controls_to_geojson(controls)
        with open("controls.geojson", "w") as write_file:
            json.dump(controls_js, write_file)

        overview, waypoints = get_trip(start, controls)
        # logging.debug("Trip: %s, %s", overview, waypoints)
        save_overview(overview, "trip-overview.geojson")

    waypoints_js = waypoints_to_geojson(waypoints)
    with open("trip-waypoints.geojson", "w") as write_file:
        json.dump(waypoints_js, write_file)

    # TODO: Write GPX route https://www.topografix.com/gpx_manual.asp

    save_point(start, "trip-start.geojson")
